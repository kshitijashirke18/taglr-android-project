package com.taglr.android.ui_fragment;


import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.taglr.android.R;
import com.taglr.android.adapters.SpecificationAdapter;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Plavaga Engineer on 14/2/17.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class FragmentSpecifications extends Fragment {

    private LinearLayout vLinearContainer;
    private RecyclerView vSpecificationRecycler;

    private Map<String, Map<String, String>> specifiactionList;
    private Map<String, String> featureList;
    private Map<String, String> generalList;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        View view = inflater.inflate(R.layout.activity_specifications, container, false);


        specifiactionList = new HashMap<>();
        featureList = new HashMap<>();
        generalList = new HashMap<>();

        featureList.put("1", "1 GB RAM|16 GB ROM");
        featureList.put("2", "4.7 inch Retina HD Display");
        featureList.put("3", "8 MP Primary Camera|1.2 MP front");
        featureList.put("4", "Li-ion Battery");
        featureList.put("5", "8 chip with 64 bit architecture and m8 Motion co-processor");
        specifiactionList.put("Features", featureList);


        generalList.put("Sales Package", "Handset,Lightning Charger,Quick Start Guide,Datacable,Headphone");
        generalList.put("Model Number", "A1586");
        generalList.put("MOdel Name", "iphone 6");
        generalList.put("Color", "Space Grey");
        generalList.put("Browse Type", "Smartphones");
        generalList.put("Sim Type", "Single Sim");
        generalList.put("Hybrid Sim Slot", "No");
        generalList.put("TouchScreen", "yes");
        generalList.put("OTG compatible", "No");
        specifiactionList.put("General", generalList);

        vLinearContainer = (LinearLayout) view.findViewById(R.id.as_linear);
        vSpecificationRecycler = (RecyclerView) view.findViewById(R.id.as_recycler);
        vSpecificationRecycler.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        vSpecificationRecycler.setLayoutManager(layoutManager);

        addSpecificationToAdapter();


        return view;

    }


    private void addSpecificationToAdapter() {

        SpecificationAdapter specificationAdapter = new SpecificationAdapter(getActivity(), R.layout.item_feature, specifiactionList);
        vSpecificationRecycler.setAdapter(specificationAdapter);


    }


}
