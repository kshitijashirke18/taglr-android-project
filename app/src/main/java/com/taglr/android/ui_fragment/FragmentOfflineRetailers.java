package com.taglr.android.ui_fragment;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.taglr.android.R;
import com.taglr.android.adapters.OfflineRetailAdapter;

/**
 * Created by Plavaga Engineer on 9/1/17.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class FragmentOfflineRetailers extends Fragment {
    private Context mContext;
    private RecyclerView offlineRecycler;
    private static final String TAG = "FragmentOfflineRetailers";

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        View view = inflater.inflate(R.layout.activity_fragmentofflineretailers, container, false);

        offlineRecycler = (RecyclerView) view.findViewById(R.id.offlineretailrecycler);
        offlineRecycler.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        offlineRecycler.setLayoutManager(layoutManager);

        addOfflineRetailToAdapter();

        return view;
    }

    private void addOfflineRetailToAdapter() {


        RecyclerView.Adapter adapter = new OfflineRetailAdapter(getActivity(), R.layout.item_product);
        offlineRecycler.setAdapter(adapter);


    }

}
