package com.taglr.android.ui_fragment;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.taglr.android.R;
import com.uncopt.android.widget.text.justify.JustifiedTextView;


/**
 * Created by Plavaga Engineer on 14/2/17.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class FragmetOtherInfo extends Fragment {
    private JustifiedTextView vDescription;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        View view = inflater.inflate(R.layout.activity_otherinfo, container, false);
        vDescription = (JustifiedTextView) view.findViewById(R.id.ao_description);
        vDescription.setText("Warranty Summary:1 year manufacturer summary");
        return view;

    }
}
