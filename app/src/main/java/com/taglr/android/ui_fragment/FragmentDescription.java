package com.taglr.android.ui_fragment;


import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.taglr.android.R;
import com.uncopt.android.widget.text.justify.JustifiedTextView;

/**
 * Created by Plavaga Engineer on 14/2/17.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class FragmentDescription extends Fragment {
    private JustifiedTextView vDescription;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        View view = inflater.inflate(R.layout.activity_descriptions, container, false);
        vDescription = (JustifiedTextView) view.findViewById(R.id.ad_description);
        vDescription.setText("Sneakers have become an important part of hip hop (primarily Pumas, Nike, and Adidas) and rock 'n roll (Converse, Macbeth) cultures since the 1970s. Hip hop artists sign million dollar deals with major brands such as Nike, Adidas or Puma to promote their shoes.[15][citation needed] Sneaker collectors, called \"Sneakerheads\", use sneakers as fashionable items. Artistically-modified sneakers can sell for upwards of $1000 at exclusive establishments like Saks Fifth Avenue.[16] In 2005 a documentary, Just for Kicks, about the sneaker phenomenon and history was released.");
        return view;

    }

}
