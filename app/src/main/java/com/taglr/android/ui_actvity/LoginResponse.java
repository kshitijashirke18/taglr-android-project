package com.taglr.android.ui_actvity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by swapnil on 24/03/17.
 */

public class LoginResponse {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("accessToken")
    @Expose
    private String accessToken;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("refreshToken")
    @Expose
    private String refreshToken;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
