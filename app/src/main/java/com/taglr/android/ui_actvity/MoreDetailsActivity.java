package com.taglr.android.ui_actvity;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.Window;

import com.taglr.android.R;
import com.taglr.android.adapters.PagerTabAdapter;
import com.taglr.android.util.FontLoader;

/**
 *   Created by Plavaga Engineer.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class MoreDetailsActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Context mContext;
    private AppCompatTextView iconShare, iconRs;
    private Typeface FontAwsomeTypeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_more_details);
        this.mContext = this;

        if (FontAwsomeTypeface == null) {
            FontAwsomeTypeface = FontLoader.getFontAwesome(mContext);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        iconShare = (AppCompatTextView) findViewById(R.id.amd_share);
        iconRs = (AppCompatTextView) findViewById(R.id.amd_itemrs);
        iconShare.setTypeface(FontAwsomeTypeface);
        iconShare.setText(mContext.getString(R.string.fa_share));
        iconRs.setTypeface(FontAwsomeTypeface);
        iconRs.setText(mContext.getString(R.string.fa_inr));


        tabLayout.addTab(tabLayout.newTab().setText(mContext.getString(R.string.descriptions)));
        tabLayout.addTab(tabLayout.newTab().setText(mContext.getString(R.string.specifications)));
        tabLayout.addTab(tabLayout.newTab().setText(mContext.getString(R.string.other_info)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        tabLayout.setTabTextColors(ContextCompat.getColor(mContext, R.color.colortext), ContextCompat.getColor(mContext, R.color.colorBlack));

        //Creating our pager adapter
        PagerTabAdapter adapter = new PagerTabAdapter(getSupportFragmentManager(), tabLayout.getTabCount());

        //Adding adapter to pager
        viewPager.setAdapter(adapter);


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition(), false);
                tabLayout.getTabAt(tab.getPosition()).select();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {


            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {


            }
        });


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position, false);
                tabLayout.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


    }
}
