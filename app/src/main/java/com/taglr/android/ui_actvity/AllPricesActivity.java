package com.taglr.android.ui_actvity;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.taglr.android.R;
import com.taglr.android.adapters.OfflinePriceAdapter;
import com.taglr.android.adapters.OnlinePriceAdapter;
import com.taglr.android.util.FontLoader;

/**
 *   Created by Plavaga Engineer.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class AllPricesActivity extends AppCompatActivity {

    private static final String TAG = "AllPricesActivity";
    private Context mContext;
    private RecyclerView recyclerView1, recyclerView2;
    private ActionBar mSupportActionBar;
    private RelativeLayout relativeWishlist, reslativeBuyNow;
    private TextView vWishlist, vBuyNow;
    private Typeface FontMaterialTypeface, FontAwsomeTypeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_all_prices);
        mContext = this;


        if (FontMaterialTypeface == null) {
            FontMaterialTypeface = FontLoader.getFontMaterial(mContext);
        }

        if (FontAwsomeTypeface == null) {
            FontAwsomeTypeface = FontLoader.getFontAwesome(mContext);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSupportActionBar = getSupportActionBar();
        mSupportActionBar.setDisplayHomeAsUpEnabled(true);


        recyclerView1 = (RecyclerView) findViewById(R.id.cap_recycler1);
        recyclerView2 = (RecyclerView) findViewById(R.id.cap_recycler2);
        relativeWishlist = (RelativeLayout) findViewById(R.id.alp_wishlist);
        reslativeBuyNow = (RelativeLayout) findViewById(R.id.alp_buynow);
        vBuyNow = (TextView) findViewById(R.id.alp_tvbuynow);
        vWishlist = (TextView) findViewById(R.id.alp_tvwishlist);


        recyclerView1.setHasFixedSize(true);
        recyclerView2.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(mContext);
        recyclerView1.setLayoutManager(layoutManager1);
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(mContext);
        recyclerView2.setLayoutManager(layoutManager2);

        addOnlinePriceToAdapter();
        addOfflinePriceToAdapter();

        relativeWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                vWishlist.setTextColor(ContextCompat.getColor(mContext, R.color.colorblue));

                Drawable d = ContextCompat.getDrawable(getApplicationContext(), R.drawable.drawable_relative_color_selector);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    relativeWishlist.setBackground(d);
                }

                vBuyNow.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));

                reslativeBuyNow.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorLIghtgreen));

            }
        });

        reslativeBuyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                vBuyNow.setTextColor(ContextCompat.getColor(mContext, R.color.colorblue));
                Drawable d = ContextCompat.getDrawable(getApplicationContext(), R.drawable.drawable_relative_color_selector);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    reslativeBuyNow.setBackground(d);
                }
                vWishlist.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                relativeWishlist.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorLIghtgreen));

                reslativeBuyNow.setClickable(false);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext, R.style.MyDialogTheme);
                LayoutInflater li = LayoutInflater.from(mContext);
                final View promptsView = li.inflate(R.layout.popup_alert, null);

                AppCompatTextView vIconCall = (AppCompatTextView) promptsView.findViewById(R.id.pa_iconcall);
                AppCompatTextView vIconWhatsApp = (AppCompatTextView) promptsView.findViewById(R.id.pa_iconwhatsapp);
                AppCompatTextView vIconLocation = (AppCompatTextView) promptsView.findViewById(R.id.pa_iconlocation);
                AppCompatTextView vIconSaveLocation = (AppCompatTextView) promptsView.findViewById(R.id.pa_iconsavelocation);
                AppCompatTextView vIconRs = (AppCompatTextView) promptsView.findViewById(R.id.pa_itemrs);
                AppCompatTextView vCost = (AppCompatTextView) promptsView.findViewById(R.id.pa_itemcost);
                AppCompatTextView vPhno = (AppCompatTextView) promptsView.findViewById(R.id.pa_phno);
                AppCompatTextView vWhatsappPhno = (AppCompatTextView) promptsView.findViewById(R.id.pa_whatsappphno);
                AppCompatTextView vDirection = (AppCompatTextView) promptsView.findViewById(R.id.pa_Direction);
                AppCompatTextView vSaveLater = (AppCompatTextView) promptsView.findViewById(R.id.pa_tvsavelater);

                vIconCall.setTypeface(FontMaterialTypeface);
                vIconCall.setText(R.string.mi_call);
                vIconWhatsApp.setTypeface(FontAwsomeTypeface);
                vIconWhatsApp.setText(R.string.fa_whatsapp);
                vIconLocation.setTypeface(FontMaterialTypeface);
                vIconLocation.setText(R.string.mi_location);
                vIconSaveLocation.setTypeface(FontMaterialTypeface);
                vIconSaveLocation.setText(R.string.mi_location);
                vIconRs.setTypeface(FontAwsomeTypeface);
                vIconRs.setText(R.string.fa_inr);
                vPhno.setPaintFlags(vPhno.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                vWhatsappPhno.setPaintFlags(vWhatsappPhno.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                vDirection.setPaintFlags(vDirection.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                vSaveLater.setPaintFlags(vDirection.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

                alertDialogBuilder.setView(promptsView);
                alertDialogBuilder.setCancelable(false);

//                alertDialogBuilder.setPositiveButton("Apply", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        reslativeBuyNow.setClickable(true);
//
//                    }
//                });

                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        reslativeBuyNow.setClickable(true);
                    }
                });
                // Create the AlertDialog object and return it
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();


            }
        });


    }

    private void addOnlinePriceToAdapter() {


        RecyclerView.Adapter adapter = new OnlinePriceAdapter(mContext, R.layout.item_all_prices);
        recyclerView1.setAdapter(adapter);


    }


    private void addOfflinePriceToAdapter() {


        RecyclerView.Adapter adapter = new OfflinePriceAdapter(mContext, R.layout.item_all_prices);
        recyclerView2.setAdapter(adapter);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.product_list_main, menu);
        // Retrieve the SearchView and plug it into SearchManagerfinal
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        return true;
    }


}
