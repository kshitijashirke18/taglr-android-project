package com.taglr.android.ui_actvity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.taglr.android.R;
import com.taglr.android.adapters.TrendingProductAdapter;
import com.taglr.android.util.AppConstants;
import com.taglr.android.util.FontLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Plavaga Engineer on 27/1/17.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class HomeActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,View.OnClickListener{

    private static final String TAG = "HomeActivity";
    private Context mContext;
    private RecyclerView trendingRecycler;
    private AppCompatTextView vRightArrow, vSeeAll;
    private AppCompatTextView vIconTag, vIconFav, vIconSettings, vIconSearch;
    private Typeface FontMaterialTypeface, FontAwsomeTypeface;
    private RelativeLayout vRelativeButtonLayout, vTopTRelativelayout;
    private LinearLayout vLinear1, vLinear2;
    private String[] trendingArray = {"MacBook Pro", "Nike Air Max 2017", "Iphone7", "Teflon", "Nike", "Max", "Life Style", "Mobiles", "Kitchen Stuffs", "Curtains"};
    private List<RelativeLayout> relativeLayoutList;
    private Map<String, Integer> trendingTextMap;
    //    private List<String> trendingArrayList;
    private boolean flag = false;
    private GoogleApiClient googleApiClient;
    private static int REQ_CODE=9001;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mContext = this;
        relativeLayoutList = new ArrayList<>();
        trendingTextMap = new HashMap<>();


        if (FontMaterialTypeface == null) {
            FontMaterialTypeface = FontLoader.getFontMaterial(mContext);
        }

        if (FontAwsomeTypeface == null) {
            FontAwsomeTypeface = FontLoader.getFontAwesome(mContext);
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(mContext, R.color.colorMidBlue));
        }


        vRightArrow = (AppCompatTextView) findViewById(R.id.ah_rightarrow);
        vSeeAll = (AppCompatTextView) findViewById(R.id.ah_seeall);
        vIconFav = (AppCompatTextView) findViewById(R.id.ah_iconfav);
        vIconSearch = (AppCompatTextView) findViewById(R.id.ah_iconsearch);
        vIconSettings = (AppCompatTextView) findViewById(R.id.ah_iconsettings);
        vIconTag = (AppCompatTextView) findViewById(R.id.ah_icontag);
        vRightArrow.setTypeface(FontMaterialTypeface);
        vRightArrow.setText(R.string.mi_rightarrow);
        vIconFav.setTypeface(FontMaterialTypeface);
        vIconFav.setText(R.string.mi_filledfav);
        vIconSearch.setTypeface(FontMaterialTypeface);
        vIconSearch.setText(R.string.mi_search);
        vIconSettings.setTypeface(FontMaterialTypeface);
        vIconSettings.setText(R.string.mi_settings);
        vIconTag.setTypeface(FontAwsomeTypeface);
        vIconTag.setText(R.string.fa_tag);
        vRelativeButtonLayout = (RelativeLayout) findViewById(R.id.ah_relativebtn);
        vTopTRelativelayout = (RelativeLayout) findViewById(R.id.ah_topfirstrelative);
        vLinear1 = (LinearLayout) findViewById(R.id.ah_linear1);
        vLinear2 = (LinearLayout) findViewById(R.id.ah_linear2);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        findViewById(R.id.btn_sign_out).setOnClickListener(this);
        findViewById(R.id.btn_log_out).setOnClickListener(this);

        Log.d(TAG, "getDefaultDisplay width" + width);
        Log.d(TAG, "getDefaultDisplay height" + height);

        generateAllWidth();


        //Google
        GoogleSignInOptions googleSignInOptions=new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleApiClient=new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,googleSignInOptions)
                .build();

//        //signout
//        vIconSettings.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                signOut();
//            }
//        });


        vSeeAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProductActivity.class);
                intent.putExtra("Trending Product", AppConstants.TITLE);
                startActivity(intent);
            }
        });

        vRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProductActivity.class);
                intent.putExtra("Trending Product", AppConstants.TITLE);
                startActivity(intent);
            }
        });

        vIconSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SearchBarActivity.class);
                startActivity(intent);
            }
        });

        //Facebook logout
        Button logout = (Button)findViewById(R.id.btn_log_out);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logOut();
                Intent login = new Intent(HomeActivity.this, LogInActivity.class);
                startActivity(login);
                finish();
            }
        });


        trendingRecycler = (RecyclerView) findViewById(R.id.ah_recycler);
        trendingRecycler.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        trendingRecycler.setLayoutManager(layoutManager);

        addTrendingProductToAdapter();
    }

    private void signOut(){

        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        updateUI(false);
                    }
                });

    }


    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            //  mStatusTextView.setText(getString(R.string.sk,acct.getDisplayName()));
            String personName = acct.getDisplayName();
            String personPhotoUrl = acct.getPhotoUrl().toString();
            String email = acct.getEmail();
            Log.e(TAG, "display name: " + acct.getDisplayName());
            Log.e(TAG, "Name: " + personName + ", email: " + email
                    + ", Image: " + personPhotoUrl);

            updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
        }
    }


    private void updateUI(boolean isSignedOut){

        {
            if (isSignedOut){
                Intent intent=new Intent(getApplicationContext(),LogInActivity.class);
                startActivity(intent);
            }


        }

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == REQ_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }



    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        super.onWindowFocusChanged(hasFocus);

        try {


            if (flag) {

                Log.d(TAG, "flag set to" + flag);
            } else {


                if (hasFocus) {

                    flag = true;

                    Log.d(TAG, "vRelativeButtonLayout width" + vRelativeButtonLayout.getWidth());

                    final int maxRelativewidth = (vRelativeButtonLayout.getWidth() * 50) / 100;

                    Log.d(TAG, "maxRelativewidth" + maxRelativewidth);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {


                            for (RelativeLayout relativeLayout : relativeLayoutList) {

                                Log.d(TAG, "relativeLayout 1 width" + relativeLayout.getWidth());
                                Log.d(TAG, "relativeLayout 1 tag" + relativeLayout.getTag());

                                if (relativeLayout.getTag() != null && relativeLayout.getWidth() != 0) {

                                    trendingTextMap.put(relativeLayout.getTag().toString(), relativeLayout.getWidth());

                                } else {
                                    Log.d(TAG, "NULL");
                                }


                            }


                            int relativeWidthFirst = vRelativeButtonLayout.getWidth();

                            int relativeWidthSecond = vRelativeButtonLayout.getWidth();

                            Log.d(TAG, "relativeWidthFirst" + relativeWidthFirst);
                            Log.d(TAG, "relativeWidthSecond" + relativeWidthSecond);


                            for (int i = 0; i < trendingArray.length; i++) {


                                Log.d(TAG, "inside display");
                                String value = trendingArray[i];


                                int width = trendingTextMap.get(value);

                                if (value != null && width != 0) {


                                    System.out.println("outside ifelse value" + value + "width" + width);


                                    int relWidthFirst = relativeWidthFirst - width;
                                    int relWidthSecond = relativeWidthSecond - width;
                                    Log.d(TAG, "relWidthFirst" + relWidthFirst + "\n" + "relWidthSecond" + relWidthSecond);


                                    if (width <= relWidthFirst && width <= maxRelativewidth) {
                                        Log.d(TAG, "inside if statememt");
                                        System.out.println("inside if value" + value + "width" + width);
                                        generateRowOneButton(value);
                                        relativeWidthFirst = relativeWidthFirst - width;

                                        Log.d(TAG, "relativeWidthFirst" + relativeWidthFirst);


                                        if (trendingArray[i].equals(value)) {
                                            Log.d(TAG, "removed" + trendingArray[i]);
                                            trendingArray[i] = null;

                                        }


                                    } else if (width <= relWidthSecond && width <= maxRelativewidth) {
                                        Log.d(TAG, "inside else statememt");
                                        System.out.println("inside else value" + value + "width" + width);
                                        generateRowTwoButton(value);
                                        relativeWidthSecond = relativeWidthSecond - width;
                                        Log.d(TAG, "relativeWidth" + relativeWidthSecond);
                                        if (trendingArray[i].equals(value)) {
                                            Log.d(TAG, "removed" + trendingArray[i]);
                                            trendingArray[i] = null;

                                        }


                                    }


                                } else {
                                    Log.d(TAG, "NULL");
                                }

                            }

                        }

                    }, 0);

                }


            }
        } catch (Exception e) {

            System.out.println("Excption" + e.getMessage());
        }

    }


    private void addTrendingProductToAdapter() {
        Log.d(TAG, "HomeActivity");
        RecyclerView.Adapter adapter = new TrendingProductAdapter(mContext, R.layout.item_trendingproduct);
        trendingRecycler.setAdapter(adapter);


    }

    private void generateRowOneButton(String itemText) {


        RelativeLayout relativeLayout = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, (int) getResources().getDimension(R.dimen.relative_height));
        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        relativeLayout.setLayoutParams(layoutParams);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            relativeLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bentrectangle_background));
        }
        TextView txt1 = new TextView(this);
        txt1.setPadding(20, 0, 20, 0);
        txt1.setText(itemText);
        txt1.setSingleLine();
        txt1.setGravity(Gravity.CENTER);
        txt1.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
        relativeLayout.addView(txt1, layoutParams);
        relativeLayout.setLayoutParams(layoutParams);
        vLinear1.addView(relativeLayout);


    }

    private void generateRowTwoButton(String itemText) {


        RelativeLayout relativeLayout = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, (int) getResources().getDimension(R.dimen.relative_height));
        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        relativeLayout.setLayoutParams(layoutParams);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            relativeLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bentrectangle_background));
        }
        TextView txt1 = new TextView(this);
        txt1.setPadding(20, 0, 20, 0);
        txt1.setText(itemText);
        txt1.setSingleLine();
        txt1.setGravity(Gravity.CENTER);
        txt1.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
        relativeLayout.addView(txt1, layoutParams);
        relativeLayout.setLayoutParams(layoutParams);
        vLinear2.addView(relativeLayout);

    }

    private void generateAllWidth() {


        for (int i = 0; i < trendingArray.length; i++) {

            RelativeLayout relativeLayout = new RelativeLayout(this);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, (int) getResources().getDimension(R.dimen.relative_height));
            layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            relativeLayout.setLayoutParams(layoutParams);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                relativeLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.transparentbackground));
            }
            TextView txt1 = new TextView(this);
            txt1.setPadding(8, 8, 8, 8);
            txt1.setText(trendingArray[i]);
            txt1.setGravity(Gravity.CENTER);
            txt1.setTextColor(Color.TRANSPARENT);
            txt1.setSingleLine();
            relativeLayout.addView(txt1, layoutParams);
            relativeLayout.setLayoutParams(layoutParams);
            relativeLayout.setTag(trendingArray[i]);
            vTopTRelativelayout.addView(relativeLayout);
            relativeLayoutList.add(relativeLayout);

        }
    }


    @Override
    public void onClick(View view) {
        signOut();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);

    }
}
