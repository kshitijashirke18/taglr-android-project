package com.taglr.android.ui_actvity;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.taglr.android.R;
import com.taglr.android.adapters.ColorListAdapter;
import com.taglr.android.adapters.SizeListAdapter;
import com.taglr.android.adapters.SliderImageAdapter;
import com.taglr.android.util.FontLoader;
import com.uncopt.android.widget.text.justify.JustifiedTextView;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Plavaga Engineer.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class ProductViewActivity extends AppCompatActivity {
    private static final String TAG = "ProductViewActivity";
    private Context mContext;
    private RelativeLayout mSaveLater, mSeePrice, mAllPrices, mColorLayout, mSizeLayout;
    private TextView iconTaglr, iconShare, mSave, mPrice, vDropDown1, vDropDown2, vDropDown3, vAlert, vIconRs;
    private JustifiedTextView vProductDescriptin;
    private Typeface FontAwsomeTypeface;
    private Typeface FontMaterialTypeface;
    private SliderImageAdapter sliderImageAdapter;
    private ViewPager mViewPager;
    private CirclePageIndicator mIndicator;
    private ActionBar mSupportActionBar;
    private AppCompatTextView mIconrs1, mIconrs2, mIconrs3, mIconrs4;
    private AppCompatTextView mReatailPrice1, mReatailPrice2, mReatailPrice3, mReatailPrice4;
    private AppCompatTextView mProductDesp1, mProductDesp2, mProductDesp3, mProductDesp4;
    private AppCompatTextView vMoreDesp, vMoreDetail;
    private List<String> featureList;
    private TableLayout tableLayout;
    private TextView vBullet;
    private TextView vFeature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_product_view);
        mContext = this;
        featureList = new ArrayList<>();


        featureList = new ArrayList();
        featureList.add("1 GB RAM|16 GB ROM");
        featureList.add("4.7 inch Retina HD Display");
        featureList.add("8 MP Primary Camera|1.2 MP front");
        featureList.add("Li-ion Battery");
        featureList.add("A8 chip with 64 bit architecture and m8 Motion co-processor");

        if (FontAwsomeTypeface == null) {
            FontAwsomeTypeface = FontLoader.getFontAwesome(mContext);
        }

        if (FontMaterialTypeface == null) {
            FontMaterialTypeface = FontLoader.getFontMaterial(mContext);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mSupportActionBar = getSupportActionBar();
        mSupportActionBar.setDisplayHomeAsUpEnabled(true);


        mSaveLater = (RelativeLayout) findViewById(R.id.apv_savelater);
        mSeePrice = (RelativeLayout) findViewById(R.id.apv_seeprices);
        mAllPrices = (RelativeLayout) findViewById(R.id.apv_realtiveallprices);
        mSizeLayout = (RelativeLayout) findViewById(R.id.apv_sizelayout);
        mColorLayout = (RelativeLayout) findViewById(R.id.apv_colorlayout);
        mSave = (TextView) findViewById(R.id.apv_textsave);
        mPrice = (TextView) findViewById(R.id.apv_textprice);
        vDropDown1 = (TextView) findViewById(R.id.apv_icon_dropdown);
        vDropDown2 = (TextView) findViewById(R.id.apv_icon_dropdown2);
        vDropDown3 = (TextView) findViewById(R.id.apv_icon_dropdown3);
        vProductDescriptin = (JustifiedTextView) findViewById(R.id.apv_description);
        vAlert = (TextView) findViewById(R.id.apv_alert);
        vAlert.setPaintFlags(vAlert.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        vIconRs = (TextView) findViewById(R.id.apv_iconrs);
        mIconrs1 = (AppCompatTextView) findViewById(R.id.apv_iconretailinr);
        mIconrs2 = (AppCompatTextView) findViewById(R.id.apv_iconretailinr2);
        mIconrs3 = (AppCompatTextView) findViewById(R.id.apv_iconretailinr3);
        mIconrs4 = (AppCompatTextView) findViewById(R.id.apv_iconretailinr4);
        mReatailPrice1 = (AppCompatTextView) findViewById(R.id.apv_retail_price);
        mReatailPrice2 = (AppCompatTextView) findViewById(R.id.apv_retail_price2);
        mReatailPrice3 = (AppCompatTextView) findViewById(R.id.apv_retail_price3);
        mReatailPrice4 = (AppCompatTextView) findViewById(R.id.apv_retail_price4);
        mProductDesp1 = (AppCompatTextView) findViewById(R.id.apv_productdescription);
        mProductDesp2 = (AppCompatTextView) findViewById(R.id.apv_productdescription2);
        mProductDesp3 = (AppCompatTextView) findViewById(R.id.apv_productdescription3);
        mProductDesp4 = (AppCompatTextView) findViewById(R.id.apv_productdescription4);
        vMoreDesp = (AppCompatTextView) findViewById(R.id.apv_moredetails);
        vMoreDesp.setPaintFlags(vMoreDesp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tableLayout = (TableLayout) findViewById(R.id.apv_tableLayout);
        vMoreDetail = (AppCompatTextView) findViewById(R.id.apv_moredetailfeatutre);
        vMoreDetail.setPaintFlags(vMoreDetail.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        mReatailPrice1.setText("999");
        mReatailPrice2.setText("999");
        mReatailPrice3.setText("999");
        mReatailPrice4.setText("999");

        mProductDesp1.setText("from Bewakoof & 3 others");
        mProductDesp2.setText("from Bewakoof & 3 others");
        mProductDesp3.setText("from Bewakoof & 3 others");
        mProductDesp4.setText("from Bewakoof & 3 others");

        mIconrs1.setTypeface(FontAwsomeTypeface);
        mIconrs1.setText(R.string.fa_inr);
        mIconrs2.setTypeface(FontAwsomeTypeface);
        mIconrs2.setText(R.string.fa_inr);
        mIconrs3.setTypeface(FontAwsomeTypeface);
        mIconrs3.setText(R.string.fa_inr);
        mIconrs4.setTypeface(FontAwsomeTypeface);
        mIconrs4.setText(R.string.fa_inr);


        vIconRs.setTypeface(FontAwsomeTypeface);
        vIconRs.setText(R.string.fa_inr);

        vProductDescriptin.setText("Nike free running shoes are optimized to fit and move with your feet to deliver a new dimension in dynamic flexibility,for a more natural experience.Nike free running shoes are optimized to fit and move with");


        iconShare = (TextView) findViewById(R.id.apv_share);
        iconShare.setTypeface(FontAwsomeTypeface);
        iconShare.setText(mContext.getString(R.string.fa_share));

        vDropDown1.setTypeface(FontMaterialTypeface);
        vDropDown1.setText(R.string.mi_downarrow);

        vDropDown2.setTypeface(FontMaterialTypeface);
        vDropDown2.setText(R.string.mi_downarrow);

        vDropDown3.setTypeface(FontMaterialTypeface);
        vDropDown3.setText(R.string.mi_downarrow);


        sliderImageAdapter = new SliderImageAdapter(this);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(sliderImageAdapter);


        mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        mIndicator.setViewPager(mViewPager);
        ((CirclePageIndicator) mIndicator).setSnap(true);

        mIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
//                Toast.makeText(ProductViewActivity.this, "Changed to page " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


        mSaveLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mSave.setTextColor(ContextCompat.getColor(mContext, R.color.colorblue));

                Drawable d = ContextCompat.getDrawable(getApplicationContext(), R.drawable.drawable_relative_color_selector);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    mSaveLater.setBackground(d);
                }

                mPrice.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));

                mSeePrice.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorLIghtgreen));
            }
        });

        mSeePrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mPrice.setTextColor(ContextCompat.getColor(mContext, R.color.colorblue));
                Drawable d = ContextCompat.getDrawable(getApplicationContext(), R.drawable.drawable_relative_color_selector);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    mSeePrice.setBackground(d);
                }
                mSave.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                mSaveLater.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorLIghtgreen));
                Intent intent = new Intent(mContext, AllPricesActivity.class);
                startActivity(intent);
            }
        });

        mAllPrices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, AllPricesActivity.class);
                startActivity(intent);
            }
        });


        mSizeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mSizeLayout.setClickable(false);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext, R.style.MyDialogTheme);
                LayoutInflater li = LayoutInflater.from(mContext);
                final View promptsView = li.inflate(R.layout.popup_size, null);

                final ListView sizeList = (ListView) promptsView.findViewById(R.id.size_listView);
                final ListView colorList = (ListView) promptsView.findViewById(R.id.color_listView);


                final SizeListAdapter sizeListAdapter = new SizeListAdapter(mContext, R.layout.item_size);
                final ColorListAdapter colorListAdapter = new ColorListAdapter(mContext, R.layout.item_color);


                sizeList.post(new Runnable() {
                    public void run() {
                        sizeList.setAdapter(sizeListAdapter);
                    }
                });
                colorList.post(new Runnable() {
                    @Override
                    public void run() {
                        colorList.setAdapter(colorListAdapter);
                    }
                });


                alertDialogBuilder.setView(promptsView);
                alertDialogBuilder.setCancelable(false);

                alertDialogBuilder.setPositiveButton("Apply", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mSizeLayout.setClickable(true);

                    }
                });

                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mSizeLayout.setClickable(true);
                    }
                });
                // Create the AlertDialog object and return it
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        mColorLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mColorLayout.setClickable(false);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext, R.style.MyDialogTheme);
                LayoutInflater li = LayoutInflater.from(mContext);
                final View promptsView = li.inflate(R.layout.popup_size, null);

                ListView sizeList = (ListView) promptsView.findViewById(R.id.size_listView);
                ListView colorList = (ListView) promptsView.findViewById(R.id.color_listView);


                SizeListAdapter sizeListAdapter = new SizeListAdapter(mContext, R.layout.item_size);
                ColorListAdapter colorListAdapter = new ColorListAdapter(mContext, R.layout.item_color);

                sizeList.setAdapter(sizeListAdapter);
                colorList.setAdapter(colorListAdapter);

                alertDialogBuilder.setView(promptsView);
                alertDialogBuilder.setCancelable(false);

                alertDialogBuilder.setPositiveButton("Apply", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        mColorLayout.setClickable(true);
                    }
                });

                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mColorLayout.setClickable(true);
                    }
                });
                // Create the AlertDialog object and return it
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });


        vMoreDesp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, MoreDetailsActivity.class);
                startActivity(intent);
            }
        });


        vMoreDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MoreDetailsActivity.class);
                startActivity(intent);

            }
        });

        init();


    }


    public void init() {


        for (int i = 0; i < featureList.size(); i++) {

            TableRow row = new TableRow(mContext);
            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT);
            row.setWeightSum(1);
            row.setLayoutParams(lp);
            vBullet = new TextView(mContext);
            vFeature = new TextView(mContext);
            vBullet.setText(R.string.bullet);
            vBullet.setTextColor(ContextCompat.getColor(mContext, R.color.colortext));
            vFeature.setText(featureList.get(i));
            vFeature.setPadding(30, 0, 0, 0);
            vFeature.setLayoutParams(new TableRow.LayoutParams(0, TableLayout.LayoutParams.WRAP_CONTENT, 1));
            vFeature.setSingleLine(false);
            row.addView(vBullet);
            row.addView(vFeature);
            tableLayout.addView(row, i);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.product_list_main, menu);
        // Retrieve the SearchView and plug it into SearchManagerfinal
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        return true;
    }
}
