package com.taglr.android.ui_actvity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.view.View;

import com.taglr.android.R;

/**
 * Created by Plavaga Engineer.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class SearchBarActivity extends AppCompatActivity {
    private Context mContext;

    String[] NikeProducts = {"Nike in Footwear", "Nike in Apparel", "Nike in Sports Asscessories", "Nike Air Max", "Nike Zoom WinFlow"};
    private AppCompatAutoCompleteTextView autoTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_bar);
        mContext = this;


        autoTextView = (AppCompatAutoCompleteTextView) findViewById(R.id.asb_autocomplete);

        autoTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProductActivity.class);
                startActivity(intent);
            }
        });
    }
}
