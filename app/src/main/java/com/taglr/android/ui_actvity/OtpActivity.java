package com.taglr.android.ui_actvity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.taglr.android.R;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.http.Path;
import retrofit.http.Url;

/**
 * Created by Plavaga Engineer.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class OtpActivity extends AppCompatActivity {

    private AppCompatTextView vOtpDescription;
    private AppCompatButton vBtnVerify;
    private TextInputLayout input_layout_otp;
    private EditText input_otp;
    private TextView resendotplabel;

    //public static final String verifyotpurl="http://ec2-34-199-201-243.compute-1.amazonaws.com:8080/web-b2c/open/user/verify-otp/";
  //  private  String resendotp="";
    private String phonenumber="";
    private APIService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        input_layout_otp= (TextInputLayout) findViewById(R.id.input_layout_otp);
        input_otp= (EditText) findViewById(R.id.input_otp);
        input_otp.addTextChangedListener(new OtpActivity.MyTextWatcher(input_otp));
        resendotplabel= (TextView) findViewById(R.id.resendotplabel);

        //get phone number from register screen
        phonenumber=getIntent().getExtras().getString("username");
       // resendotp="http://ec2-34-199-201-243.compute-1.amazonaws.com:8080/web-b2c/open/retailer/resend-otp/"+phonenumber;

        vOtpDescription = (AppCompatTextView) findViewById(R.id.descriptionotp);
        vOtpDescription.setText("We have sent OTP to your mobile number. Please enter it below to proceed.");

        resendotplabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //for api(resend otp)
                final ResendOtpRequest resendOtpRequest=new ResendOtpRequest();
                resendOtpRequest.setPhonenumber(phonenumber);

                Call<ResendOtpResponse> resendOtpResponseCall=apiService.resend(phonenumber);
                resendOtpResponseCall.enqueue(new Callback<ResendOtpResponse>() {
                    @Override
                    public void onResponse(Response<ResendOtpResponse> response, Retrofit retrofit) {
                        boolean statuscode=response.isSuccess();

                        String userResponse=response.toString();
                        Log.d("ResendOtpActivity","onResponse:"+statuscode);
                        Log.d("ResendOtpActivity","onResponse"+userResponse);

//                        if(statuscode==true)
//                        {
//                            Intent intent = new Intent(getApplicationContext(), LogInActivity.class);
//                            startActivity(intent);
//                        }
//                        else
//                        {
//                            customtoastverify();
//                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.d("ResendOtpActivity","onFailure:"+t.getMessage());
                    }
                });




            }
        });

        vBtnVerify = (AppCompatButton) findViewById(R.id.ao_btnverify);
        vBtnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //call this method for validation
                submitForm();

                //for api(verify phone number)
                final OtpRequest otpRequest=new OtpRequest();
                otpRequest.setPhonenumber(phonenumber);
                otpRequest.setOtp(input_otp.getText().toString());

                Call<OtpResponse> otpResponseCall= apiService.insertotp(otpRequest);
                otpResponseCall.enqueue(new Callback<OtpResponse>() {
                    @Override
                    public void onResponse(Response<OtpResponse> response, Retrofit retrofit) {

                        boolean statuscode=response.isSuccess();

                        String userResponse=response.toString();
                        Log.d("OtpActivity","onResponse:"+statuscode);
                        Log.d("OtpActivity","onResponse"+userResponse);

                        if(statuscode==true)
                        {
                                   Intent intent = new Intent(getApplicationContext(), LogInActivity.class);
                                   startActivity(intent);
                        }
                        else
                        {
                                customtoastverify();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.d("OtpActivity","onFailure:"+t.getMessage());
                    }
                });


            }
        });//end of button

        //Api part
        Retrofit retrofit=new Retrofit.Builder().baseUrl("http://ec2-34-199-201-243.compute-1.amazonaws.com:8080/").addConverterFactory(GsonConverterFactory.create()).build();
        apiService=retrofit.create(APIService.class);

    }//end of oncreate

    //custom toast
    private void customtoastverify()
    {
        //Creating the LayoutInflater instance
        LayoutInflater li = getLayoutInflater();

        //Getting the View object as defined in the customtoast.xml file
        View layout = li.inflate(R.layout.ctotpverficationfail,
                (ViewGroup) findViewById(R.id.otpverificationfail));

        //Creating the Toast object
        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setView(layout);//setting the view of custom toast layout
        toast.show();
    }


    private void submitForm() {
        if (!validateOTP()) {
            return;
        }


    }

    private boolean validateOTP() {
        if (input_otp.getText().toString().trim().isEmpty()) {
            input_layout_otp.setError(getString(R.string.err_msg_otp));
            requestFocus(input_otp);
            return false;
        }
        else
        {
           input_layout_otp.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_otp:
                    validateOTP();
                    break;

            }
        }
    }


    @Override
    public void onBackPressed() {

                Intent intent = new Intent(this, SignUpActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                super.onBackPressed();
    }








}
