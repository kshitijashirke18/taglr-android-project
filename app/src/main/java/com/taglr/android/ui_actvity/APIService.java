package com.taglr.android.ui_actvity;


import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by swapnil on 21/03/17.
 */

public interface APIService {

    //for user registration
    @POST("web-b2c/open/user/register")
    Call<UserResponse> getResponse(@Body UserRequest userRequest);

    //For verifying phone number
    @POST("web-b2c/open/user/verify-otp/")
    Call<OtpResponse> insertotp(@Body OtpRequest otpRequest);

    //for resend otp
    @POST("web-b2c/open/user/resend-otp/{phone-number}")
    Call<ResendOtpResponse> resend(@Path("phone-number") String phonenumber);

    //for login
    @POST("web-security/api/1/b2c/security/login")
    Call<LoginResponse> login(@Body LoginRequest loginRequest);







}
