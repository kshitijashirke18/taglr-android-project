package com.taglr.android.ui_actvity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by swapnil on 24/03/17.
 */

public class LoginRequest {

    @SerializedName("userName")
    private String userName;
    @SerializedName("password")
    private String passWord;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
}
