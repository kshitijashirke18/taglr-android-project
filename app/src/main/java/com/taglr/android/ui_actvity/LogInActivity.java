package com.taglr.android.ui_actvity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookActivity;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.gson.Gson;
import com.taglr.android.R;

import org.androidannotations.annotations.OnActivityResult;

import java.util.Arrays;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class LogInActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,View.OnClickListener{

    private static final String TAG ="Hell";
    //Declaration of button,layout & edittext
    private AppCompatButton vBtnCreate;
    private EditText input_phone, input_password;
    private TextInputLayout input_layout_phone, input_layout_password;
    private TextView show;
    private ImageView Prof_Pic;
    private ProgressDialog mProgressDialog;
    private GoogleApiClient googleApiClient;
    private SignInButton signInButton;
    private ImageView facebook,twitter,googleplus;

    LoginButton mFacebookLoginButton;
    CallbackManager mCallbackManager;
    private static int REQ_CODE=9001;

    private APIService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Facebook Integration 14/03/17 4:30
        FacebookSdk.sdkInitialize(getApplicationContext());
        mCallbackManager=CallbackManager.Factory.create();
        setContentView(R.layout.activity_log_in);


        //initialization of button,layout & edittext
        vBtnCreate = (AppCompatButton) findViewById(R.id.al_btncreate);

        input_phone = (EditText) findViewById(R.id.input_phone);
        input_password = (EditText) findViewById(R.id.input_password);

        input_layout_phone = (TextInputLayout) findViewById(R.id.input_layout_phone);
        input_layout_password = (TextInputLayout) findViewById(R.id.input_layout_password);

        input_phone.addTextChangedListener(new LogInActivity.MyTextWatcher(input_phone));
        input_password.addTextChangedListener(new LogInActivity.MyTextWatcher(input_password));

        show= (TextView) findViewById(R.id.show);

        facebook= (ImageView) findViewById(R.id.facebook);
        twitter= (ImageView) findViewById(R.id.twitter);
        googleplus= (ImageView) findViewById(R.id.googleplus);
        signInButton= (SignInButton) findViewById(R.id.sign_in_button);
        mFacebookLoginButton= (LoginButton) findViewById(R.id.login_button);
        googleplus.setOnClickListener(this);
        signInButton.setOnClickListener(this);


        //facebook integration....
        mFacebookLoginButton.setReadPermissions(Arrays.asList("public_profile","email","user_friends"));
        mFacebookLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                Intent intent=new Intent(getApplicationContext(),HomeActivity.class);
                startActivity(intent);

                Toast.makeText(getApplicationContext(),"Login Successfull",Toast.LENGTH_LONG).show();

            }

            @Override
            public void onCancel() {

                Toast.makeText(getApplicationContext(),"Login Cancelled ",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                Intent intent=new Intent(getApplicationContext(),HomeActivity.class);
                startActivity(intent);

                Toast.makeText(getApplicationContext(),"Login Successfully Done",Toast.LENGTH_LONG).show();

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        //on click of facebook image
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LoginManager.getInstance().logInWithReadPermissions(LogInActivity.this, Arrays.asList("public_profile", "user_friends"));
                //  Toast.makeText(getApplicationContext(),"Button clicked ",Toast.LENGTH_SHORT).show();

            }
        });


        vBtnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                submitForm();

                final LoginRequest loginRequest=new LoginRequest();
                loginRequest.setUserName(input_phone.getText().toString());
                loginRequest.setPassWord(input_password.getText().toString());

                final Call<LoginResponse> loginResponseCall= apiService.login(loginRequest);
                loginResponseCall.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Response<LoginResponse> response, Retrofit retrofit) {

                        Gson gson=new Gson();
                        String res=gson.toJson(loginRequest);
                        boolean statuscode=response.isSuccess();
                        String userResponse=response.toString();
                        Log.d("LoginActivity","onResponse:"+statuscode);
                        Log.d("LoginActivity","onResponse"+userResponse);
                        Log.d("LoginActivity","onResponse"+res);

                        if(statuscode==true)
                        {
                            customtoastverify();
                            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                            startActivity(intent);

                        }
                        else
                        {

                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.d("LoginActivity","onFailure:"+t.getMessage());
                    }
                });
            }
        });

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),SignUpActivity.class);
                startActivity(intent);
            }
        });

        //Google
        GoogleSignInOptions googleSignInOptions=new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleApiClient=new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,googleSignInOptions)
                .build();

        Retrofit retrofit=new Retrofit.Builder().baseUrl("http://ec2-52-90-67-161.compute-1.amazonaws.com:8080/").addConverterFactory(GsonConverterFactory.create()).build();
        apiService=retrofit.create(APIService.class);


    }//end of onCreate

    private void submitForm() {
        if (!validatePhone()) {
            return;
        }

        if (!validatePassword()) {
            return;
        }

    }

    private boolean validatePhone() {
        if (input_phone.getText().toString().trim().isEmpty()) {
            input_layout_phone.setError(getString(R.string.err_msg_phone));
            requestFocus(input_phone);
            return false;
        }
        else if(input_phone.getText().toString().length()<10)
        {
            input_layout_phone.setError(getString(R.string.err_msg_digits));
            requestFocus(input_phone);
            return false;
        }
        else
        {
            input_layout_phone.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePassword() {
        if (input_password.getText().toString().trim().isEmpty()) {
            input_layout_password.setError(getString(R.string.err_msg_pass));
            requestFocus(input_password);
            return false;
        }
        else if(input_password.getText().toString().length()<6)
        {
            input_layout_password.setError(getString(R.string.err_msg_less));
            requestFocus(input_password);
            return false;
        }
        else
        {
            input_layout_password.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_phone:
                    validatePhone();
                    break;
                case R.id.input_password:
                    validatePassword();
                    break;

            }
        }
    }




    //Google Single Sign On Started from here.....

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, REQ_CODE);
    }


    private void signOut(){

        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        updateUI(false);
                    }
                });

    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            //  mStatusTextView.setText(getString(R.string.sk,acct.getDisplayName()));
            String personName = acct.getDisplayName();
            String personPhotoUrl = acct.getPhotoUrl().toString();
            String email = acct.getEmail();
            Log.e(TAG, "display name: " + acct.getDisplayName());
            Log.e(TAG, "Name: " + personName + ", email: " + email
                    + ", Image: " + personPhotoUrl);

            updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
        }
    }

    private void updateUI(boolean isSignedIn){

        {
            if (isSignedIn){
                Intent intent=new Intent(getApplicationContext(),HomeActivity.class);
                startActivity(intent);
            }


        }

    }



    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.googleplus:
                signIn();
                break;
            case 2:
                signOut();
                break;
        }

    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == REQ_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }


    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }



    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }


    private void customtoastverify()
    {
        //Creating the LayoutInflater instance
        LayoutInflater li = getLayoutInflater();

        //Getting the View object as defined in the customtoast.xml file
        View layout = li.inflate(R.layout.loginsuccessfull,
                (ViewGroup) findViewById(R.id.loginsuccessfull));

        //Creating the Toast object
        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setView(layout);//setting the view of custom toast layout
        toast.show();
    }









}   //end of main