package com.taglr.android.ui_actvity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by swapnil on 21/03/17.
 */

public class UserRequest {

    @SerializedName("username")
    private  String username;
    @SerializedName("password")
    private  String password;
    @SerializedName("confirm-password")
    private  String confirmpassword;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmpassword() {
        return confirmpassword;
    }

    public void setConfirmpassword(String confirmpassword) {
        this.confirmpassword = confirmpassword;
    }
}
