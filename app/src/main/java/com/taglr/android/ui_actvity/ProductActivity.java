package com.taglr.android.ui_actvity;

/**
 * Created by Plavaga Engineer on 12/1/17.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

import android.app.FragmentManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.taglr.android.R;
import com.taglr.android.adapters.PagerAdapter;
import com.taglr.android.util.FontLoader;

import java.util.ArrayList;
import java.util.List;


public class ProductActivity extends AppCompatActivity {
    private static final String TAG = "ListRetaileritems";
    private TextView vOnlineRetail, vOfflineRetail, vWholeSaleRetail, vIconUpsort, vIconDownSort, vTitle, vIconTaglr;
    private TextView IconSlider, IconSort;
    private Context mContext;
    private Typeface FontMaterialTypeface;
    private Typeface FontAwsomeTypeface;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private FragmentManager activity;
    private int tabCount;
    private RelativeLayout vLayoutSort, vLayoutFilter;
    private List<String> sortList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);


        setContentView(R.layout.activity_product);
        mContext = this;
        sortList = new ArrayList<>();
        sortList.add("Relevance");
        sortList.add("Popularity");
        sortList.add("Price-Low to High");
        sortList.add("Price-High to Low");
        sortList.add("Newest First");

        Intent intent = getIntent();
        String title = intent.getStringExtra("Trending Product");
        Log.d(TAG, "title" + title);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);


        tabLayout.addTab(tabLayout.newTab().setText(mContext.getString(R.string.online_retailers)));
        tabLayout.addTab(tabLayout.newTab().setText(mContext.getString(R.string.offline_retailers)));
        tabLayout.addTab(tabLayout.newTab().setText(mContext.getString(R.string.wholesellers)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        tabLayout.setTabTextColors(ContextCompat.getColor(mContext, R.color.colorGrey), ContextCompat.getColor(mContext, R.color.colorWhite));


        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        if (FontMaterialTypeface == null) {
            FontMaterialTypeface = FontLoader.getFontMaterial(mContext);
        }

        if (FontAwsomeTypeface == null) {
            FontAwsomeTypeface = FontLoader.getFontMaterial(mContext);
        }


        IconSlider = (TextView) findViewById(R.id.ap_filters);
        vIconUpsort = (TextView) findViewById(R.id.ap_iconupsort);
        vIconDownSort = (TextView) findViewById(R.id.ap_icondownsort);
        vTitle = (TextView) findViewById(R.id.toolbar_title);
        vIconTaglr = (TextView) findViewById(R.id.ap_iconTaglr);
        vLayoutSort = (RelativeLayout) findViewById(R.id.relativesort);
        vLayoutFilter = (RelativeLayout) findViewById(R.id.realativefilter);


        if (title != null) {
            Log.d(TAG, "title" + title);
            vTitle.setText(title);
        }


        vIconUpsort.setTypeface(FontMaterialTypeface);
        vIconUpsort.setText(mContext.getString(R.string.mi_uparrow));
        vIconDownSort.setTypeface(FontMaterialTypeface);
        IconSlider.setTypeface(FontMaterialTypeface);
        IconSlider.setText(mContext.getString(R.string.mi_slider));
        vIconTaglr.setTypeface(FontMaterialTypeface);
        vIconTaglr.setBackgroundResource(R.drawable.t);


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition(), false);
                tabLayout.getTabAt(tab.getPosition()).select();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {


            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {


            }
        });


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position, false);
                tabLayout.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


        vLayoutSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vLayoutSort.setClickable(false);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext, R.style.MyDialogTheme);
                LayoutInflater li = LayoutInflater.from(mContext);
                final View promptsView = li.inflate(R.layout.popup_sortby, null);

                final RadioButton radioButton = (RadioButton) promptsView.findViewById(R.id.ps_radioButton);
                final TextView vsortType = (TextView) promptsView.findViewById(R.id.ps_sortby);

                for (int i = 0; i < sortList.size(); i++) {
                    vsortType.setText(sortList.get(i));
                }


                alertDialogBuilder.setView(promptsView);
                alertDialogBuilder.setCancelable(false);

                alertDialogBuilder.setPositiveButton("Apply", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        vLayoutSort.setClickable(true);

                    }
                });

                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        vLayoutSort.setClickable(true);
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }


        });

        vLayoutFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.product_list_main, menu);
        // Retrieve the SearchView and plug it into SearchManagerfinal
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        return true;
    }


}
