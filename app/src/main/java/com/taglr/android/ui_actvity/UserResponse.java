package com.taglr.android.ui_actvity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by swapnil on 21/03/17.
 */

public class UserResponse {

    @SerializedName("code")
    @Expose
    private int code;

    @SerializedName("object")
    @Expose
    private String object;
    @SerializedName("status")
    @Expose
    private String status;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }
}
