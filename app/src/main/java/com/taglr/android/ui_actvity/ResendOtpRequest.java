package com.taglr.android.ui_actvity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by swapnil on 23/03/17.
 */

public class ResendOtpRequest {

    @SerializedName("phone-number")
    private String phonenumber;

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }
}
