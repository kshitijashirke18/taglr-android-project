package com.taglr.android.ui_actvity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by swapnil on 22/03/17.
 */

public class OtpRequest {

    @SerializedName("phone-number")
    private String phonenumber;
    @SerializedName("otp")
    private String otp;

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
