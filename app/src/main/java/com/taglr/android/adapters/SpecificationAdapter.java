package com.taglr.android.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.taglr.android.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Plavaga Engineer on 15/2/17.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class SpecificationAdapter extends RecyclerView.Adapter<SpecificationAdapter.ViewHolder> {

    private static final String TAG = "SpecificationAdapter";
    private Context mContext;
    private final int item_featureid;
    private Map<String, Map<String, String>> featureList;

    public SpecificationAdapter(FragmentActivity activity, int item_feature, Map<String, Map<String, String>> specifiactionList) {
        this.mContext = activity;
        this.item_featureid = item_feature;
        this.featureList = specifiactionList;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_feature, parent, false);
        SpecificationAdapter.ViewHolder viewHolder = new SpecificationAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Set<String> keyList = featureList.keySet();
        List<String> list = new ArrayList<String>(keyList);

        holder.vTitle.setText(list.get(position));

        Map<String, String> currentMap = featureList.get(list.get(position));
        Set<String> innerKeyList = currentMap.keySet();
        ArrayList<String> itemList = new ArrayList<>();
        itemList.addAll(innerKeyList);
        Collections.reverse(itemList);

        for (String innerKey : itemList) {

            TableRow row = new TableRow(mContext);
            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT);
            row.setWeightSum(1);
            row.setLayoutParams(lp);
            TextView vBullet = new TextView(mContext);
            TextView vFeature = new TextView(mContext);

            if (list.get(position).equalsIgnoreCase("Features")) {
                vBullet.setText(R.string.bullet);
            } else {
                vBullet.setText(innerKey);
            }

            vBullet.setPadding(20, 0, 0, 0);
            vBullet.setTextColor(ContextCompat.getColor(mContext, R.color.colortext));
            vFeature.setText(currentMap.get(innerKey));
            vFeature.setPadding(30, 0, 0, 0);
            vFeature.setLayoutParams(new TableRow.LayoutParams(0, TableLayout.LayoutParams.WRAP_CONTENT, 1));
            vFeature.setSingleLine(false);
            row.addView(vBullet);
            row.addView(vFeature);
            holder.tableLayout.addView(row);
        }
    }

    @Override
    public int getItemCount() {
        return featureList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView vTitle;
        private TableLayout tableLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            vTitle = (AppCompatTextView) itemView.findViewById(R.id.if_title);
            tableLayout = (TableLayout) itemView.findViewById(R.id.if_tablelayout);
        }

    }
}
