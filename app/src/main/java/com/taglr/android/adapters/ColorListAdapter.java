package com.taglr.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.taglr.android.R;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Plavaga Engineer.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */


public class ColorListAdapter extends BaseAdapter {

    private static LayoutInflater inflater;

    private final Context mContext;
    private String[] mColors = {"Grey", "Purple", "Black", "White"};
    private Integer[] colorImage = {R.drawable.colors, R.drawable.colors, R.drawable.colors, R.drawable.colors};
    private final int listItemViewId;

    public ColorListAdapter(Context context, int listItemViewId) {
        this.mContext = context;
        this.listItemViewId = listItemViewId;


        if (inflater == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

    }

    @Override
    public int getCount() {
        if (mColors == null) {
            return 0;
        } else {
            return mColors.length;
        }
    }

    @Override
    public Object getItem(int position) {
        if (mColors == null) {
            return null;
        } else {
            return mColors[position];
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View returnedView = null;

        returnedView = inflater.inflate(listItemViewId, null);

        final TextView itemName = (TextView) returnedView.findViewById(R.id.ic_colorname);
        final CircleImageView circleImageView = (CircleImageView) returnedView.findViewById(R.id.ic_colorimage);
        itemName.setText(mColors[position].toString());
        circleImageView.setImageResource(colorImage[position]);


        return returnedView;
    }

}
