package com.taglr.android.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.taglr.android.ui_fragment.FragmentDescription;
import com.taglr.android.ui_fragment.FragmentSpecifications;
import com.taglr.android.ui_fragment.FragmetOtherInfo;

/**
 * Created by Plavaga Engineer on 14/2/17.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class PagerTabAdapter extends FragmentStatePagerAdapter {
    public int tabCount;

    public PagerTabAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                FragmentDescription fr = new FragmentDescription();
                return fr;
            case 1:
                FragmentSpecifications fr1 = new FragmentSpecifications();
                return fr1;
            case 2:
                FragmetOtherInfo fr2 = new FragmetOtherInfo();
                return fr2;
            default:
                return null;

        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
