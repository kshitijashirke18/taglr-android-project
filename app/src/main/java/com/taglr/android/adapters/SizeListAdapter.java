package com.taglr.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.taglr.android.R;

/**
 * Created by Plavaga Engineer.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */


public class SizeListAdapter extends BaseAdapter {

    private static LayoutInflater inflater;
    private final Context mContext;
    private String[] mSizes = {"UK5/US7", "UK8/US9", "UK5/US7", "UK5/US7"};
    private final int listItemViewId;
    private boolean mIsEditEnabled;

    public SizeListAdapter(Context context, int listItemViewId) {
        this.mContext = context;
        this.listItemViewId = listItemViewId;

        if (inflater == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


    }

    @Override
    public int getCount() {
        if (mSizes == null) {
            return 0;
        } else {
            return mSizes.length;
        }
    }

    @Override
    public Object getItem(int position) {
        if (mSizes == null) {
            return null;
        } else {
            return mSizes[position];
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View returnedView = null;
        returnedView = inflater.inflate(listItemViewId, null);
        final TextView itemName = (TextView) returnedView.findViewById(R.id.is_size);
        itemName.setText(mSizes[position]);
        return returnedView;
    }

}
