package com.taglr.android.adapters;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.taglr.android.R;
import com.taglr.android.ui_actvity.ProductViewActivity;
import com.taglr.android.util.FontLoader;

/**
 * Created by Plavaga Engineer on 10/1/17.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */
public class WholesaleRetailAdapter extends RecyclerView.Adapter<WholesaleRetailAdapter.ViewHolder> {

    private static final String TAG = "WholesaleRetailAdapter";
    private Context mContext;
    private Integer item_retail;

    private Integer[] ProductImage = {R.drawable.shoes, R.drawable.shoes, R.drawable.shoes, R.drawable.shoes, R.drawable.shoes, R.drawable.shoes, R.drawable.shoes, R.drawable.shoes, R.drawable.shoes, R.drawable.shoes};
    private String[] Productname = {"Black Sneakers", "Black Sneakers", "Black Sneakers", "Black Sneakers", "Black Sneakers", "Black Sneakers", "Black Sneakers", "Black Sneakers", "Black Sneakers", "Black Sneakers"};
    private String[] ProductDescription = {"from Bewakoof & 2 more", "from Bewakoof & 2 more", "from Bewakoof & 2 more", "from Bewakoof & 2 more", "from Bewakoof & 2 more", "from Bewakoof & 2 more", "from Bewakoof & 2 more", "from Bewakoof & 2 more", "from Bewakoof & 2 more", "from Bewakoof & 2 more"};


    public WholesaleRetailAdapter(Activity activity, int item_retailer) {
        this.mContext = activity;
        this.item_retail = item_retailer;
    }

    @Override
    public WholesaleRetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(WholesaleRetailAdapter.ViewHolder holder, int position) {
        holder.ProductImage.setImageResource(ProductImage[position]);
        holder.ProductName.setText(Productname[position]);
        holder.ProductDescription.setText(ProductDescription[position]);
        holder.MrpPrice.setText("1120");
        holder.RetailPrice.setText("999");
        holder.MrpPrice.setPaintFlags(holder.MrpPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.RetailerCardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProductViewActivity.class);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return ProductImage.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ProductImage;
        private TextView ProductName, ProductDescription, IconFavourite, MrpPrice, RetailPrice, IconMrpInr, IconRetailInr;
        private CardView RetailerCardview;
        private Typeface FontMaterialTypeface;
        private Typeface FontAwsomeTypeface;

        public ViewHolder(View itemView) {
            super(itemView);
            ProductImage = (ImageView) itemView.findViewById(R.id.ip_image);
            ProductName = (TextView) itemView.findViewById(R.id.ip_imagename);
            ProductDescription = (TextView) itemView.findViewById(R.id.ip_productdescription);
            IconFavourite = (TextView) itemView.findViewById(R.id.ip_favourite);
            MrpPrice = (TextView) itemView.findViewById(R.id.ip_mrp_price);
            RetailPrice = (TextView) itemView.findViewById(R.id.ip_retail_price);
            IconMrpInr = (TextView) itemView.findViewById(R.id.ip_iconinr);
            IconRetailInr = (TextView) itemView.findViewById(R.id.ip_iconretailinr);
            RetailerCardview = (CardView) itemView.findViewById(R.id.reatilercardview);


            if (FontMaterialTypeface == null) {
                FontMaterialTypeface = FontLoader.getFontMaterial(mContext);
            }

            if (FontAwsomeTypeface == null) {
                FontAwsomeTypeface = FontLoader.getFontAwesome(mContext);
            }

            IconFavourite.setTypeface(FontMaterialTypeface);
            IconFavourite.setText(mContext.getString(R.string.mi_fav));
            IconMrpInr.setTypeface(FontAwsomeTypeface);
            IconMrpInr.setText(mContext.getString(R.string.fa_inr));
            IconMrpInr.setPaintFlags(IconMrpInr.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            IconRetailInr.setTypeface(FontAwsomeTypeface);
            IconRetailInr.setText(mContext.getString(R.string.fa_inr));
        }
    }
}
