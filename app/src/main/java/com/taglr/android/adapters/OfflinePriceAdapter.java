package com.taglr.android.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.taglr.android.R;
import com.taglr.android.util.FontLoader;

/**
 * Created by Plavaga Engineer on 20/1/17.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 **/


public class OfflinePriceAdapter extends RecyclerView.Adapter<OfflinePriceAdapter.ViewHolder> {
    private Boolean[] RadioType = {true, false, false, false, true};
    private static final String TAG = "OfflinePriceAdapter";
    private Context mContext;
    private int item_price;
    private Long item_id;
    private Integer[] CompanyImage = {R.drawable.flipkart_small, R.drawable.snapdeal_small, R.drawable.amazon_small, R.drawable.snapdeal_small, R.drawable.snapdeal_small};
    private String[] ProductPrice = {"1599", "1799", "2199", "2199", "2199"};
    private String[] Comment = {"2km", "5.5km", "5.5km", "5.5km", "5.5km"};

    public OfflinePriceAdapter(Context mContext, int item_all_prices) {
        this.mContext = mContext;
        this.item_price = item_all_prices;
    }

    @Override
    public OfflinePriceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_all_prices, parent, false);
        OfflinePriceAdapter.ViewHolder viewHolder = new OfflinePriceAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(OfflinePriceAdapter.ViewHolder holder, int position) {
        holder.ComapnyImage.setImageResource(CompanyImage[position]);
        holder.Comment.setText(Comment[position]);
        holder.ProductPrice.setText(ProductPrice[position]);
    }


    @Override
    public int getItemCount() {
        return ProductPrice.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ComapnyImage;
        private TextView Comment, ProductPrice, IconInr;
        private RadioButton RadioBtn;
        private Typeface FontMaterialTypeface, FontAwsomeTypeface;

        public ViewHolder(View itemView) {
            super(itemView);

            ComapnyImage = (ImageView) itemView.findViewById(R.id.iap_imageView);
            Comment = (TextView) itemView.findViewById(R.id.iap_description);
            ProductPrice = (TextView) itemView.findViewById(R.id.iap_price);
            RadioBtn = (RadioButton) itemView.findViewById(R.id.iap_radioButton);
            IconInr = (TextView) itemView.findViewById(R.id.iap_iconrs);

            if (FontMaterialTypeface == null) {
                FontMaterialTypeface = FontLoader.getFontMaterial(mContext);
            }

            if (FontAwsomeTypeface == null) {
                FontAwsomeTypeface = FontLoader.getFontAwesome(mContext);
            }

            IconInr.setTypeface(FontAwsomeTypeface);
            IconInr.setText(R.string.fa_inr);

        }
    }
}
