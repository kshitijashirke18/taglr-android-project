package com.taglr.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.taglr.android.R;

/**
 * Created by Plavaga Engineer on 18/1/17.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class SliderImageAdapter extends android.support.v4.view.PagerAdapter {

    private int[] mResources = {
            R.drawable.shoes,
            R.drawable.shoes,
            R.drawable.shoes
    };

    private Context mContext;
    private LayoutInflater mLayoutInflater;

    public SliderImageAdapter(Context context) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mResources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.item_productview, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.ipv_image);
        imageView.setImageResource(mResources[position]);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
