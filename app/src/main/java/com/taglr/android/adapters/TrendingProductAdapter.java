package com.taglr.android.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.taglr.android.R;
import com.taglr.android.util.FontLoader;

/**
 * Created by Plavaga Engineer on 27/1/17.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class TrendingProductAdapter extends RecyclerView.Adapter<TrendingProductAdapter.ViewHolder> {

    private static final String TAG = "TrendingProductAdapter";
    private Context mContext;
    private int item_trending;
    private Long item_id;
    private Integer[] ProductImage = {R.drawable.shoes, R.drawable.shoes, R.drawable.shoes, R.drawable.shoes, R.drawable.shoes, R.drawable.shoes, R.drawable.shoes, R.drawable.shoes, R.drawable.shoes, R.drawable.shoes};
    private String[] Productname = {"Black Sneakers", "Black Sneakers", "Black Sneakers", "Black Sneakers", "Black Sneakers", "Black Sneakers", "Black Sneakers", "Black Sneakers", "Black Sneakers", "Black Sneakers"};
    private String[] ProductDescription = {"from Bewakoof & 2 more", "from Bewakoof & 2 more", "from Bewakoof & 2 more", "from Bewakoof & 2 more", "from Bewakoof & 2 more", "from Bewakoof & 2 more", "from Bewakoof & 2 more", "from Bewakoof & 2 more", "from Bewakoof & 2 more", "from Bewakoof & 2 more"};

    public TrendingProductAdapter(Context mContext, int item_trendingproduct) {
        this.mContext = mContext;
        this.item_trending = item_trendingproduct;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trendingproduct, parent, false);
        TrendingProductAdapter.ViewHolder viewHolder = new TrendingProductAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.ProductImage.setImageResource(ProductImage[position]);
        holder.ProductName.setText(Productname[position]);
        holder.ProductDescription.setText(ProductDescription[position]);
        holder.RetailPrice.setText("999");
    }

    @Override
    public int getItemCount() {
        return ProductImage.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView ProductImage;
        private AppCompatTextView ProductName, ProductDescription, RetailPrice, IconRetailInr;
        private CardView TrendingCardview;
        private Typeface FontMaterialTypeface;
        private Typeface FontAwsomeTypeface;

        public ViewHolder(View itemView) {
            super(itemView);

            ProductImage = (ImageView) itemView.findViewById(R.id.it_imageview);
            ProductName = (AppCompatTextView) itemView.findViewById(R.id.it_product_name);
            ProductDescription = (AppCompatTextView) itemView.findViewById(R.id.it_productdescription);
            RetailPrice = (AppCompatTextView) itemView.findViewById(R.id.it_retail_price);
            IconRetailInr = (AppCompatTextView) itemView.findViewById(R.id.it_iconretailinr);
            TrendingCardview = (CardView) itemView.findViewById(R.id.it_cardview);

            if (FontMaterialTypeface == null) {
                FontMaterialTypeface = FontLoader.getFontMaterial(mContext);
            }

            if (FontAwsomeTypeface == null) {
                FontAwsomeTypeface = FontLoader.getFontAwesome(mContext);
            }


            IconRetailInr.setTypeface(FontAwsomeTypeface);
            IconRetailInr.setText(mContext.getString(R.string.fa_inr));

        }
    }
}
