package com.taglr.android.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.taglr.android.ui_fragment.FragmentOfflineRetailers;
import com.taglr.android.ui_fragment.FragmentOnlineRetailers;
import com.taglr.android.ui_fragment.FragmentWholesaleRetailers;


/**
 * Created by Plavaga Engineer on 16/1/17.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {

    public int tabCount;

    public PagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }


    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                FragmentOnlineRetailers fr = new FragmentOnlineRetailers();
                return fr;
            case 1:
                FragmentOfflineRetailers fr1 = new FragmentOfflineRetailers();
                return fr1;
            case 2:
                FragmentWholesaleRetailers fr2 = new FragmentWholesaleRetailers();
                return fr2;
            default:
                return null;

        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
