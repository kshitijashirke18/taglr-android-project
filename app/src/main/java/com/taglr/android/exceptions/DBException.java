package com.taglr.android.exceptions;

/**
 * Created by Plavaga Engineer.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class DBException extends AppException {
    public DBException() {
    }

    public DBException(String detailMessage) {
        super(detailMessage);
    }

    public DBException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public DBException(Throwable throwable) {
        super(throwable);
    }
}
