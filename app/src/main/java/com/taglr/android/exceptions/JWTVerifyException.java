package com.taglr.android.exceptions;

/**
 * Created by Plavaga Engineer.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class JWTVerifyException extends AppException {
    public JWTVerifyException() {
    }

    public JWTVerifyException(String detailMessage) {
        super(detailMessage);
    }

    public JWTVerifyException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public JWTVerifyException(Throwable throwable) {
        super(throwable);
    }
}
