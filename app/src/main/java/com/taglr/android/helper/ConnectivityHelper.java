/**
 * Class Name:			ConnectivityHelper
 * Created On:			11:25:37 AM, Apr 30, 2014
 * <p/>
 * Copyright (c) 2012 Plavaga Software Solutions (P) Ltd. All rights reserved.
 * <p/>
 * Use is subject to license terms.
 */

package com.taglr.android.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Plavaga Engineer.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class ConnectivityHelper {

    static boolean isConnected;
    static int type;

    public static boolean isConnected(Context c) {

        NetworkInfo activeNetwork = checkConnection(c);
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        return isConnected;
    }

    /**
     * @param c
     */
    private static NetworkInfo checkConnection(Context c) {

        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork;
    }

    public static int getConnectionType(Context c) {

        NetworkInfo activeNetwork = checkConnection(c);
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        if (isConnected) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                type = 2;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                type = 1;
            }
        }
        return type;
    }

}
