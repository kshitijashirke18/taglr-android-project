package com.taglr.android.sync.remote;

import android.content.Context;

/**
 * Created by Plavaga Engineer. on 14/2/17.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public interface ProductDetailsService {

    String getProductDetails(int productId, Context context);

}
