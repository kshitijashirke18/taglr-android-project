package com.taglr.android.sync.remote.impl;

import android.content.Context;

import com.taglr.android.sync.remote.ProductDetailsService;
import com.taglr.android.util.AppLog;
import com.taglr.android.util.RestApiConnector;
import com.taglr.android.util.RestApiUrlGenerator;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Plavaga Engineer on 14/2/17.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class ProductDetailsServiceImpl implements ProductDetailsService {
    private static final String TAG = "ProductDetailsServiceImpl";

    private Context mContext;
    private String productUrl, productName, productMpn, lowestPriceRetailer, imageUrl1, groupId, brandName;
    private int productStatus, lowestPrice, mproductId;

    private static final ProductDetailsService productDetailsService = new ProductDetailsServiceImpl();


    public ProductDetailsServiceImpl() {

    }

    public static ProductDetailsService getInstance() {
        return productDetailsService;
    }


    @Override

    public String getProductDetails(int productId, Context context) {


        String responseJson = RestApiConnector.get(RestApiUrlGenerator.getProductDetailsUrl(productId), null);
        AppLog.info(TAG, "responseJson :: " + responseJson);
        try {

            JSONObject responseObject = new JSONObject(responseJson);
            JSONObject jsonObject = responseObject.optJSONObject("object");
            productUrl = jsonObject.getString("productUrl");
            productName = jsonObject.getString("productName");
            productMpn = jsonObject.getString("productMpn");
            productStatus = jsonObject.getInt("productStatus");
            lowestPriceRetailer = jsonObject.getString("lowestPriceRetailer");
            lowestPrice = jsonObject.getInt("lowestPrice");
            imageUrl1 = jsonObject.getString("imageUrl1");
            groupId = jsonObject.getString("groupId");
            brandName = jsonObject.getString("brandName");
            mproductId = jsonObject.getInt("productId");


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return responseJson;


    }
}
