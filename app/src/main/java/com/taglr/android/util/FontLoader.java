package com.taglr.android.util;

import android.content.Context;
import android.graphics.Typeface;

/**
 *   Created by Plavaga Engineer.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class FontLoader {


    private static Typeface fontAwesome;
    private static Typeface fontMaterial;

    private FontLoader() {

    }


    public static Typeface getFontAwesome(Context context) {

        if (fontAwesome == null) {
            fontAwesome = Typeface.createFromAsset(context.getAssets(), "fontawesome-webfont.ttf");
        }
        return fontAwesome;
    }

    public static Typeface getFontMaterial(Context context) {

        if (fontMaterial == null) {
            fontMaterial = Typeface.createFromAsset(context.getAssets(), "MaterialIcons-Regular.ttf");
        }
        return fontMaterial;
    }

}
