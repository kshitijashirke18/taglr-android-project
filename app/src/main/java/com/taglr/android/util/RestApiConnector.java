package com.taglr.android.util;

import android.os.AsyncTask;

import com.taglr.android.exceptions.AppException;
import com.taglr.android.exceptions.AuthException;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Set;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.security.cert.X509Certificate;

/**
 *   Created by Plavaga Engineer.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class RestApiConnector {

    private static final String TAG = RestApiConnector.class.getSimpleName();

    private static final String JSON_CODE = "code";
    private static final String JSON_MESSAGE = "message";
    private static final String JSON_DEV_MESSAGE = "developerMessage";
    private static final String JSON_STATUS = "status";

    private static final String CONTENT_TYPE = "Content-Type";
    private static final String X_MOCK_ACCESS = "x-mock-access";
    private static final String APPLICATION_JSON = "application/json";
    private static final String X_MOCK_ACCESS_VALUE = "c3d9f7e2ac18fc124df983dc844ec7";

    private static final String MULTIPART_FORMDATA = "multipart/form-data;boundary=" + "===" + System.currentTimeMillis() + "===";

    private static final String HTTP_POST = "POST";
    private static final String HTTP_GET = "GET";
    private static final String HTTP_DELETE = "DELETE";
    private static final String HTTP_PUT = "PUT";

    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String BEARER = "Bearer ";


    static {
        disableSslVerification();
    }

    public RestApiConnector() {

    }

    private static void disableSslVerification() {

        try {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {

                @SuppressWarnings("unused")
                public void checkClientTrusted(X509Certificate[] certs, String authType) {

                }

                @SuppressWarnings("unused")
                public void checkServerTrusted(X509Certificate[] certs, String authType) {

                }

                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {

                    // TODO Auto-generated method stub

                }

                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {

                    // TODO Auto-generated method stub

                }

                public java.security.cert.X509Certificate[] getAcceptedIssuers() {

                    return null;
                }
            }};

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {

                public boolean verify(String hostname, SSLSession session) {

                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            if (AppConstants.DEBUG_ENABLED) {
                AppLog.error(TAG, "NoSuchAlgorithmException", e);
            }
        } catch (KeyManagementException e) {
            if (AppConstants.DEBUG_ENABLED) {
                AppLog.error(TAG, "KeyManagementException", e);
            }
        }
    }

    public static String get(String url, String requestBody) {
        return httpsConnection(url, requestBody, HTTP_GET, null, APPLICATION_JSON, X_MOCK_ACCESS_VALUE, null);
    }

    public static String get(String url, String requestBody, String authToken) {
        return httpsConnection(url, requestBody, HTTP_GET, authToken, APPLICATION_JSON, X_MOCK_ACCESS_VALUE, null);
    }

    public static String post(String url, String requestBody) {
        return httpsConnection(url, requestBody, HTTP_POST, null, APPLICATION_JSON, X_MOCK_ACCESS_VALUE, null);
    }

    public static String post(String url, String requestBody, String authToken) {
        return httpsConnection(url, requestBody, HTTP_POST, authToken, APPLICATION_JSON, X_MOCK_ACCESS_VALUE, null);
    }


    public static String postMultipart(String url, String requestBody, String authToken, Set<File> files) {
        return httpsConnection(url, requestBody, HTTP_POST, authToken, MULTIPART_FORMDATA, X_MOCK_ACCESS_VALUE, files);
    }

    public static String delete(String url, String requestBody, String authToken) {
        return httpsConnection(url, requestBody, HTTP_DELETE, authToken, APPLICATION_JSON, X_MOCK_ACCESS_VALUE, null);
    }


    public static String delete(String url, String requestBody) {
        return httpsConnection(url, requestBody, HTTP_DELETE, null, APPLICATION_JSON, X_MOCK_ACCESS_VALUE, null);
    }

//    public static String getAsync(String url, String requestBody) {
//        String result = null;
//        return new AsyncRestTask(result).execute(url, requestBody, HTTP_GET, null);
//    }
//
//    public static String getAsync(String url, String requestBody, String authToken) {
//        return httpsConnectionAsync(url, requestBody, HTTP_GET, authToken);
//    }
//
//    public static String postAsync(String url, String requestBody) {
//        return httpsConnectionAsync(url, requestBody, HTTP_POST, null);
//    }
//
//    public static String postAsync(String url, String requestBody, String authToken) {
//        return httpsConnectionAsync(url, requestBody, HTTP_POST, authToken);
//    }

    public static String put(String url, String requestBody) {
        return httpsConnection(url, requestBody, HTTP_PUT, null, APPLICATION_JSON, X_MOCK_ACCESS_VALUE, null);
    }

    public static String put(String url, String requestBody, String authToken) {
        return httpsConnection(url, requestBody, HTTP_PUT, authToken, APPLICATION_JSON, X_MOCK_ACCESS_VALUE, null);
    }

    private static String httpsConnection(String address, String query, String method, String authToken, String contentType, String xMockAccess, Set<File> files) {

        BufferedReader bufferedReader = null;
        String jsonResult = null;
        URL url;
        HttpURLConnection connection = null;

        try {
//            if(MULTIPART_FORMDATA.equalsIgnoreCase(contentType)) {
//                MultipartUtility multipart = new MultipartUtility(address, "UTF-8", authToken);
//
//                multipart.addFormField("documentJson", query);
//
//                if(files != null) {
//                    for (File file : files) {
//                        multipart.addFilePart("file", file);
//                    }
//                }
//
//                List<String> response = multipart.finish();
//                if(response != null && response.size() > 0) {
//                    jsonResult = response.get(0);
//                }
//
//            }
//            else {
            url = new URL(address);

            connection = (HttpURLConnection) url.openConnection();
            if (authToken != null) {
                connection.setRequestProperty(AUTHORIZATION_HEADER, BEARER + authToken);
            }
            connection.setRequestProperty(CONTENT_TYPE, contentType);

            connection.setRequestProperty(X_MOCK_ACCESS, xMockAccess);

            connection.setRequestMethod(method);
            connection.setDoInput(true);
            if (HTTP_POST.equalsIgnoreCase(method)) {
                connection.setDoOutput(true);
            }

            if (query != null) {
                DataOutputStream output = new DataOutputStream(connection.getOutputStream());
                output.writeBytes(query);
                output.close();
            }

            connection.connect();
            //TODO verify if JSON. if not, then bail out!
            //connection.getContentType();
            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            if (bufferedReader == null) {
                throw new AppException("No response from server");
            }
            jsonResult = IOUtils.toString(bufferedReader);
            //  }

        } catch (MalformedURLException e) {
            AppLog.info(TAG, "MalformedURLException", e);
        } catch (IOException e) {
            int responseCode = 0;
            try {
                responseCode = connection.getResponseCode();
            } catch (IOException e1) {
                AppLog.info(TAG, "IOException", e1);
            }
            if (responseCode == 500) {
                AppLog.error(TAG, "IOException", e);
            } else {
                AppLog.info(TAG, "IOException", e);
            }
            try {
                InputStream is = connection.getErrorStream();
                if (is == null) {
                    throw new AppException("No response from server");
                }
                bufferedReader = new BufferedReader(new InputStreamReader(is));

                if (bufferedReader == null) {
                    throw new AppException("No response from server");
                }
                jsonResult = IOUtils.toString(bufferedReader);
                if (jsonResult == null) {
                    throw new AppException("No response from server");
                }
                try {
                    JSONObject jsonObject = new JSONObject(jsonResult);

                    //int errorCode = jsonObject.getInt(JSON_CODE);
                    String errorMessage = jsonObject.getString(JSON_MESSAGE);
                    //String errorDevMessage = jsonObject.getString(JSON_DEV_MESSAGE);
                    String errorStatus = jsonObject.getString(JSON_STATUS);

//                AppLog.debug(TAG, "responseCode: " + responseCode + ", errorCode: " + errorCode + ", errorMessage: " + errorMessage +
//                        ", errorDevMessage: " + errorDevMessage + ", errorStatus: " + errorStatus);

                    AppLog.debug(TAG, "responseCode: " + responseCode + ", errorMessage: " + errorMessage +
                            ", errorStatus: " + errorStatus);

                    if (AppConstants.REPORTING_ENABLED) {
                        //   Instabug.log("responseCode: " + responseCode + ", errorMessage: " + errorMessage +
                        //          ", errorStatus: " + errorStatus);
                    }

                    if ("NOT_FOUND".equals(errorStatus)) {
                        throw new AppException(errorMessage);
                    } else if ("UNAUTHORIZED".equals(errorStatus)) {
                        throw new AuthException(errorMessage);
                    }
                } catch (JSONException ex) {
                    throw new AppException(e);
                }

            } catch (MalformedURLException ex) {
                AppLog.info(TAG, "MalformedURLException", ex);
                throw new AppException(ex);
            } catch (IOException ex) {
                AppLog.info(TAG, "AuthException", ex);
                throw new AuthException(ex);
            }
        } catch (RuntimeException e) {
            AppLog.info(TAG, e.getMessage(), e);
        } catch (Exception e) {
            AppLog.error(TAG, e);
        }

        return jsonResult;

    }


    private static class AsyncRestTask extends AsyncTask<String, Void, String> {

        private String mResult;

        public AsyncRestTask(String result) {
            this.mResult = result;
        }

        @Override
        protected String doInBackground(String... params) {
            String address = params[0];
            String query = params[1];
            String method = params[2];
            String authToken = params[3];
            return httpsConnection(address, query, method, authToken, APPLICATION_JSON, X_MOCK_ACCESS_VALUE, null);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            mResult = result;
        }
    }

    public static void main(String[] args) {

    }

}
