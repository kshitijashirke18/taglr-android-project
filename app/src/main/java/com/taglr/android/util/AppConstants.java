package com.taglr.android.util;

/**
 *   Created by Plavaga Engineer.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class AppConstants {


    public static final boolean DEBUG_ENABLED = true;
    public static final boolean REPORTING_ENABLED = true;


    public static final String TITLE = "Trending Product";


}