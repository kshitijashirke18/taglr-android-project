package com.taglr.android.util;

/**
 *   Created by Plavaga Engineer.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class RestApiUrlGenerator {

    private static final String PROTOCOL = "https://";

    private static final String SERVER_DOMAIN = "apimint.com";
    private static final String SERVER_PORT = ":443";

    static final String URL_SERVER_PRODUCTDETAILSPATH = "/mock/TAGLR%20B2C/v2/product-details";
    static final String URL_AUTH_SERVER_PATH = "/mock/TAGLR/TAGLR%20B2C/v2/";


    public static String getProductDetailsUrl(int productId) {
        return PROTOCOL + SERVER_DOMAIN + SERVER_PORT + URL_SERVER_PRODUCTDETAILSPATH;

    }

    public static void main(String[] args) {


    }
}
