package com.taglr.android.util;

import android.util.Log;


/**
 *   Created by Plavaga Engineer.
 * Copyright © 2017 Plavaga Software Solutions Pvt Ltd. All rights reserved.
 */

public class AppLog {

    public static void verbose(String tag, String message) {
        if (AppConstants.DEBUG_ENABLED) {
            Log.v(tag, "" + message);
        }
    }

    public static void verbose(String tag, String message, Throwable throwable) {
        if (AppConstants.DEBUG_ENABLED) {
            Log.v(tag, "" + message, throwable);
        }
    }

    public static void debug(String tag, String message) {
        if (AppConstants.DEBUG_ENABLED) {
            Log.d(tag, "" + message);
        }
    }

    public static void debug(String tag, String message, Throwable throwable) {
        if (AppConstants.DEBUG_ENABLED) {
            Log.d(tag, "" + message, throwable);
        }
    }

    public static void info(String tag, String message) {
        if (AppConstants.DEBUG_ENABLED) {
            Log.i(tag, "" + message);
        }
    }

    public static void info(String tag, String message, Throwable throwable) {
        if (AppConstants.DEBUG_ENABLED) {
            Log.i(tag, "" + message, throwable);
        }
    }

    public static void warn(String tag, String message) {
        if (AppConstants.DEBUG_ENABLED) {
            Log.w(tag, "" + message);
        }
    }

    public static void warn(String tag, String message, Throwable throwable) {
        if (AppConstants.DEBUG_ENABLED) {
            Log.w(tag, "" + message, throwable);
        }
    }

    public static void error(String tag, String message) {
        if (AppConstants.DEBUG_ENABLED) {
            Log.e(tag, "" + message);
        }
    }

    public static void error(String tag, Throwable throwable) {
        if (AppConstants.REPORTING_ENABLED && throwable != null) {

        }
        if (AppConstants.DEBUG_ENABLED && throwable != null) {
            Log.e(tag, "" + throwable.getMessage(), throwable);
        }
    }

    public static void error(String tag, String message, Throwable throwable) {
        if (AppConstants.REPORTING_ENABLED && throwable != null) {

        }
        if (AppConstants.DEBUG_ENABLED) {
            Log.e(tag, "" + message, throwable);
        }
    }

    public static void fatal(String tag, String message) {
        if (AppConstants.DEBUG_ENABLED) {
            Log.wtf(tag, "" + message);
        }
    }

    public static void fatal(String tag, Throwable throwable) {
        if (AppConstants.DEBUG_ENABLED) {
            Log.wtf(tag, throwable);
        }
    }

    public static void fatal(String tag, String message, Throwable throwable) {
        if (AppConstants.DEBUG_ENABLED) {
            Log.wtf(tag, "" + message, throwable);
        }
    }
}
